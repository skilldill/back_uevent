from flask import Flask
from flask_restful import  Api

from recources import Sample, Authorization, DetectorRegion



app = Flask(__name__)

api = Api(app)
api.prefix = "/v1"

api.add_resource(Sample, "/", "/start")
api.add_resource(Authorization, "/signin/", "signin")
api.add_resource(DetectorRegion, "/cords/", "cords")

if __name__ == "__main__":
    app.run(debug=True)
