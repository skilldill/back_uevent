from geopy.geocoders import Yandex
from .constants import KEY_WORDS


def detect_region(latitude=None, longitude=None):
    geolocator = Yandex()
    locations = geolocator.reverse(f"{latitude}, {longitude}")

    primary_location = locations[0]
    location_parts = primary_location.address.split(',')

    return dict(
        district=location_parts[0],
        city=location_parts[1],
        country=location_parts[2]
    )


def find_groups(location=None, vk=None):
    groups = vk.groups.search(q=location["city"], sort=4)

    return groups


def get_groups_short_data(groups=None, count=None):
    groups_ids = []

    count = count if count and len(groups) > count else len(groups)

    print(count)

    for i in range(count):
        group = dict(
            id=groups[i]["id"],
            screen_name=groups[i]["screen_name"],
            name=groups[i]["name"],
            url=f"https://vk.com/{groups[i]['screen_name']}"
        )
        groups_ids.append(group)

    return groups_ids


def get_posts_of_wall(group=None, vk=None):
    posts = vk.wall.get(owner_id=-group["id"])
    short_posts = []

    for post in posts["items"]:
        if len(post["text"]) > 60:
            short_post = dict(
                id=post["id"],
                url=f"{group['url']}?w=wall{post['from_id']}_{post['id']}",
                text=post["text"]
            )

            short_posts.append(short_post)

    return dict(url=group["url"], name=group["name"], posts=short_posts)


def get_walls_of_groups(groups=None, vk=None):
    walls = []

    for group in groups:
        try:
            wall = get_posts_of_wall(group=group, vk=vk)
            walls.append(wall)
        except:
            continue

    return walls