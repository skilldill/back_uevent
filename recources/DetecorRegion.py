from flask_restful import Resource, abort
from flask import request
import vk_api
from . import VkControl, utils

#Example query: /v1/cords/?latitude=57&longitude=57 or /v1/cords/?location=Тюмень

class DetectorRegion(Resource):
    vk = VkControl.vk

    def get(self):
        cords = request.args.to_dict()

        if "latitude" in cords and "longitude" in cords:
            location = utils.detect_region(cords["latitude"], cords["longitude"])
        else:
            location = dict(city=cords["location"])

        try:
            groups = utils.find_groups(location=location, vk=self.vk.vk_api)
            shorts = utils.get_groups_short_data(groups=groups["items"], count=10)
            walls = utils.get_walls_of_groups(groups=shorts, vk=self.vk.vk_api)

            return walls

        except Exception:
            return abort(403)