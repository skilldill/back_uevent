from flask import request
from flask_restful import Resource, abort
from . import VkControl
import vk_api

class Authorization(Resource):
    vk = VkControl.vk

    def post(self):
        data = request.get_json() if request.is_json else None

        try:
            self.vk(data)
            return {"status": "succsess"}

        except vk_api.exceptions.VkApiError:
            return abort(401)
        #
        # self.vk.vk_api.wall.post(message="TEST")
        # news = self.vk.vk_api.newsfeed.get(filters=['note'])
        # groups = self.vk.vk_api.groups.search(q="Тюмень", type="event")

        #group = self.vk.vk_api.groups.get(owner_id=-143702508, count=5)
        #post = self.vk.vk_api.wall.getById(posts="143702508_32370614")

        #return data



