from flask_restful import Resource
from .import VkControl

class Sample(Resource):
    vk = VkControl.vk

    def get(self):
        groups = self.vk.vk_api.groups.search(q="Тюмень", type="event")
        return groups