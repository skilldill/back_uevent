from vk_api import VkApi


class VkControl:
    def __init__(self):
        self.session = None
        self.vk_api = None

    def __call__(self, data_user):
        self.session = VkApi(
            login=data_user['login'],
            password=data_user['password']
        )

        self.session.auth()
        self.vk_api = self.session.get_api()

vk = VkControl()