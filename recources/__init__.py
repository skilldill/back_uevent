from .Sample import Sample
from .Authorization import Authorization
from .DetecorRegion import DetectorRegion

from .VkControl import VkControl

vk = VkControl()